#include "mainwindow.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QTextEdit>
#include <QAction>
#include <QMenuBar>
#include <QMenu>
#include <QToolBar>
#include <QDockWidget>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    this->resize(900, 600); // resize window
    isChange = false; // default change status

    text = new QTextEdit(this); // create instance of text area
    this->setCentralWidget(text); // move to central

    QMenuBar *mBar = menuBar();

    // add file menu
    pFile = mBar->addMenu("&File");

    // add actions
    newFile = pFile->addAction("New");
    newFile->setShortcut(tr("CTRL+N"));
    connect(newFile, &QAction::triggered, this, &MainWindow::on_new);

    openFile = pFile->addAction("Open");
    openFile->setShortcut(tr("CTRL+O"));
    connect(openFile, &QAction::triggered, this, &MainWindow::on_open);

    saveFile = pFile->addAction("Save");
    saveFile->setShortcut(tr("CTRL+S"));
    connect(saveFile, &QAction::triggered, this, &MainWindow::on_save);

    exit = pFile->addAction("Exit");
    exit->setShortcut(tr("CTRL+Q"));
    connect(exit, &QAction::triggered, this, &MainWindow::on_quit);


    // add edit menu
    pEdit = mBar->addMenu("&Edit");

    // add actions
    copyEdit = pEdit->addAction("Copy");
    copyEdit->setShortcut(tr("CTRL+C"));
    connect(copyEdit, &QAction::triggered, this, &MainWindow::on_copy);

    pasteEdit = pEdit->addAction("Paste");
    pasteEdit->setShortcut(tr("CTRL+V"));
    connect(pasteEdit, &QAction::triggered, this, &MainWindow::on_paste);

    cutEdit = pEdit->addAction("Cut");
    cutEdit->setShortcut(tr("CTRL+X"));
    connect(cutEdit, &QAction::triggered, this, &MainWindow::on_cut);

    selectAllEdit = pEdit->addAction("Select All");
    selectAllEdit->setShortcut(tr("CTRL+A"));
    connect(selectAllEdit, &QAction::triggered, this, &MainWindow::on_allSelect);


    // add help menu
    pHelp = mBar->addMenu("&Help");

    // add actions
    aboutEditor = pHelp->addAction("About");
    connect(aboutEditor, &QAction::triggered, this, &MainWindow::on_aboutEditor);

    howtoUse = pHelp->addAction("Usage");
    connect(howtoUse, &QAction::triggered, this, &MainWindow::on_howToUse);

}


void MainWindow::on_new()
{
    if(isChange)
    {
        QMessageBox::information(this, "Tip", "File is not saved");
    }
    else
    {
        text->setText(""); // new file is blank
    }
}


void MainWindow::on_open()
{
    if(isChange)
    {
        QMessageBox::information(this, "Tip", "File is not saved");
    }
    else
    {
        filenameString = QFileDialog::getOpenFileName(this, "Open");
        if(filenameString == NULL)
            return ;
        FILE *pFile = fopen(filenameString.toStdString().data(), "r+");
        if(pFile == NULL)
            return ;
        char buf[1024];
        QString str;
        while(!feof(pFile))
        {
            fgets(buf, sizeof(buf), pFile);
            str += buf;
        }
        text->setText(str);
        fclose(pFile);
    }

}


void MainWindow::on_save()
{
     filenameString = QFileDialog::getSaveFileName(this, "Save");
     if(filenameString == nullptr)
         return ;
     FILE *pf = fopen(filenameString.toStdString().data(), "w+");
     if(pf == nullptr)
         return ;

     QString str = text->toPlainText();
     fputs(str.toStdString().data(), pf);
     fclose(pf);
     isChange = true;
}


void MainWindow::on_quit()
{
    if(!isChange)
    {
        int ret = QMessageBox::question(this, "Question", "Save file?");
        switch(ret)
        {
        case QMessageBox::No:
            this->close();
            break;
        case QMessageBox::Yes:
            on_save();
            this->close();
            break;
        default:
            break;
        }
    }
    else
    {
        this->close();
    }
}


void MainWindow::on_copy()
{
    text->copy();
}


void MainWindow::on_paste()
{
    text->paste();
}


void MainWindow::on_cut()
{
    text->cut();
}


void MainWindow::on_allSelect()
{
    text->selectAll();
}


void MainWindow::on_howToUse()
{
    QMessageBox::information(this, "Usage", "You can guess how use it");
}


void MainWindow::on_aboutEditor()
{
    QMessageBox::information(this, "About", "This notepad was by Qt5.14.2,thanks Qt and QtCreator.");
}


void MainWindow::on_changed()
{
    isChange = true;
}


MainWindow::~MainWindow()
{
}

