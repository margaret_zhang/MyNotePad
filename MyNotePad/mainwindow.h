#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTextEdit>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    bool isChange;
    QString filenameString; // file name

    QTextEdit *text; // edit area

    QMenu *pFile; // file menu

    QAction *newFile; // new a file
    QAction *openFile; // open a file
    QAction *saveFile; // save a file
    QAction *exit; // exit

    QMenu *pEdit; // edit menu

    QAction *copyEdit; // copy edit
    QAction *pasteEdit; // paste edit
    QAction *cutEdit; // cut edit
    QAction *selectAllEdit; // select all

    QMenu *pHelp; // help menu

    QAction *aboutEditor; // about
    QAction *howtoUse; // usage

private slots:
    void on_new();
    void on_open();
    void on_save();
    void on_quit();

    void on_copy();
    void on_paste();
    void on_cut();
    void on_allSelect();

    void on_howToUse();
    void on_aboutEditor();
    void on_changed();

};
#endif // MAINWINDOW_H
